package cz.fit.dpo.cbi.arithmetic;

import cz.fit.dpo.cbi.arithmetic.elements.CloseBracketOperation;
import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;
import cz.fit.dpo.cbi.arithmetic.elements.OpenBracketOperation;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Represents the Binary operations like + - etc.
 */
public abstract class BinaryOperator extends ArithmeticExpression {

    private ArithmeticExpression firstOperand;
    private ArithmeticExpression secondOperand;

    public BinaryOperator(ArithmeticExpression firstOperand, ArithmeticExpression secondOperand) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    public ArrayList<ExpressionElement> getInOrderElementRepresentation(){

        ArrayList<ExpressionElement> e = new ArrayList();

        e.add(new OpenBracketOperation());

        e.addAll(getFirstOperand().getInOrderElementRepresentation());

        e.add(getOperationMark());

        e.addAll(getSecondOperand().getInOrderElementRepresentation());

        e.add(new CloseBracketOperation());

        return e;
    }

    public ArrayList<ExpressionElement> getPostOrderElementRepresentation(){

        ArrayList<ExpressionElement> e = new ArrayList();

        e.addAll(getFirstOperand().getPostOrderElementRepresentation());

        e.addAll(getSecondOperand().getPostOrderElementRepresentation());

        e.add(getOperationMark());

        return e;
    }

    public ArithmeticExpression getFirstOperand() {
        return firstOperand;
    }

    public ArithmeticExpression getSecondOperand() {
        return secondOperand;
    }

    public abstract ExpressionElement getOperationMark();

}
