package cz.fit.dpo.cbi.arithmetic;

import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;
import cz.fit.dpo.cbi.arithmetic.elements.SubstractOperation;

import java.util.Iterator;

/**
 * Represents - operation
 */
public class SubstractOperator extends BinaryOperator {
    
    public SubstractOperator(ArithmeticExpression firstOperand, ArithmeticExpression secondOperand) {
        super(firstOperand, secondOperand);
    }

    @Override
    public Integer evaluate() {
        return getFirstOperand().evaluate() - getSecondOperand().evaluate();
    }

    @Override
    public ExpressionElement getOperationMark() {
        return new SubstractOperation();
    }


}
