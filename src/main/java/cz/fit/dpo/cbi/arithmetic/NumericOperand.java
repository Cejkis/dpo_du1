package cz.fit.dpo.cbi.arithmetic;

import cz.fit.dpo.cbi.arithmetic.elements.*;
import cz.fit.dpo.cbi.arithmetic.elements.Number;
import cz.fit.dpo.cbi.arithmetic.iterator.InOrderIterator;
import cz.fit.dpo.cbi.arithmetic.iterator.PostOrderIterator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Represents number in the arithmetic expression
 */
public class NumericOperand extends ArithmeticExpression {

    private Integer value;

    public NumericOperand(Integer value) {
        this.value = value;
    }

    @Override
    public Integer evaluate() {
        return value;
    }

    @Override
    public ArrayList<ExpressionElement> getInOrderElementRepresentation() {
        ArrayList<ExpressionElement> e = new ArrayList<>();
        e.add(new Number(value));
        return e;
    }

    @Override
    public ArrayList<ExpressionElement> getPostOrderElementRepresentation() {
        ArrayList<ExpressionElement> e = new ArrayList<>();
        e.add(new Number(value));
        return e;
    }


}
