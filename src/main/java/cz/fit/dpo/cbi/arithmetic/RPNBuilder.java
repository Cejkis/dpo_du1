package cz.fit.dpo.cbi.arithmetic;

import cz.fit.dpo.cbi.arithmetic.elements.*;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

/**
 * Created by cejkis on 22.11.15.
 */
public class RPNBuilder { // vyrobi "strom" arithmetic expression

    Stack<ArithmeticExpression> zasobnik;

    public RPNBuilder() {
        zasobnik = new Stack<>();
    }

    public void buildNext(String s){

        ArithmeticExpression a = null;

        if(s.equals("+") ){
            a = new AddOperator(zasobnik.pop(), zasobnik.pop());
        }
        if(s.equals("-")){
            ArithmeticExpression b = zasobnik.pop();
            a = new SubstractOperator(zasobnik.pop(), b);
        }

        if(a==null){
            Integer i = Integer.valueOf(s); // throws number format exception
            a = new NumericOperand(i);
        }

        zasobnik.push(a);
    }

    public ArithmeticExpression getResult(){

        if(zasobnik.size() != 1)
            throw new IllegalArgumentException("Size of stack should be 1. It's " + zasobnik.size() );

        return zasobnik.pop();
    }


}
