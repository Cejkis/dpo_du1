package cz.fit.dpo.cbi.arithmetic;

import java.util.ArrayList;
import java.util.Iterator;

import cz.fit.dpo.cbi.arithmetic.elements.CloseBracketOperation;
import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;
import cz.fit.dpo.cbi.arithmetic.elements.OpenBracketOperation;
import cz.fit.dpo.cbi.arithmetic.iterator.InOrderIterator;
import cz.fit.dpo.cbi.arithmetic.iterator.PostOrderIterator;

public abstract class ArithmeticExpression {

    public abstract Integer evaluate();
    
    public Iterator<ExpressionElement> getInOrderIterator(){
        InOrderIterator i = new InOrderIterator(getInOrderElementRepresentation());
        return i;
    }

    public Iterator<ExpressionElement> getPostOrderIterator(){
        InOrderIterator i = new InOrderIterator(getPostOrderElementRepresentation());
        return i;
    }

    public abstract ArrayList<ExpressionElement> getInOrderElementRepresentation();

    public abstract ArrayList<ExpressionElement> getPostOrderElementRepresentation();

}
