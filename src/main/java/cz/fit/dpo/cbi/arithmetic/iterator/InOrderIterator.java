package cz.fit.dpo.cbi.arithmetic.iterator;

import java.util.ArrayList;
import java.util.Iterator;

import cz.fit.dpo.cbi.arithmetic.ArithmeticExpression;
import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;
import cz.fit.dpo.cbi.arithmetic.elements.OpenBracketOperation;

public class InOrderIterator implements Iterator<ExpressionElement> {

    ArrayList<ExpressionElement> ElementRepresentation;
    int pointer;

    public InOrderIterator(ArrayList<ExpressionElement> elementRepresentation) {
        ElementRepresentation = elementRepresentation;
        pointer = 0;
    }

    @Override
    public boolean hasNext() {
        return pointer < ElementRepresentation.size();
    }

    @Override
    public ExpressionElement next() {
        return ElementRepresentation.get(pointer++);
    }

    @Override
    public void remove() {
        throw  new UnsupportedOperationException();
        //ElementRepresentation.remove(pointer);
    }
}
