package cz.fit.dpo.cbi.arithmetic;

import cz.fit.dpo.cbi.arithmetic.RPNBuilder;

/**
 * Created by cejkis on 22.11.15.
 */
public class Director {

    RPNBuilder builder;

    public Director(RPNBuilder builder) {
        this.builder = builder;
    }

    public void Construct(String st){

        if(builder == null){
            throw new NullPointerException("builder is null");
        }

        String[] expressions = st.split(" ");

        for (String s: expressions){
            builder.buildNext(s);
        }

    }


}
