package cz.fit.dpo.cbi;

import cz.fit.dpo.cbi.arithmetic.ArithmeticExpression;
import cz.fit.dpo.cbi.arithmetic.iterator.InOrderIterator;
import cz.fit.dpo.cbi.arithmetic.iterator.PostOrderIterator;

import java.util.Iterator;

/**
 * Printer for {@link ArithmeticExpression}s. It can print inOrder notation (3 + 1) or postOrder
 * notation (3 1 +).
 *
 * PostOrder is RPN (Reverse Polish Notation) in fact. See wiki for more information.
 *
 */
public class ArithmeticExpressionPrinter {

    private ArithmeticExpression expression;

    public ArithmeticExpressionPrinter(ArithmeticExpression expression) {
        this.expression = expression;
    }

    /**
     * Print an expression in classical notation, e.g. (3+1).     
     * The "(" and ")" is used to preserve priorities.     
     * @return String in classical "inOrder" format.
     */
    public String printInOrder() {

        String s = new String();

        InOrderIterator i = new InOrderIterator(expression.getInOrderElementRepresentation());

        while(i.hasNext()){
            s= s.concat(i.next().stringValue());
        }

        return s;
    }    

    /**
     * Print an expression in RPN notation, e.g. 3 1 +.     
     * Please note, the "(" and ")" are no longer necessary, because RPN does not need them.
     * @return string in "postOrder" (RPN) format.
     */
    public String printPostOrder() {
        String s = new String();

        InOrderIterator i = new InOrderIterator(expression.getPostOrderElementRepresentation());

        while(i.hasNext()){
            s= s.concat(i.next().stringValue() + " ");
        }

       s = s.substring(0, s.length()-1);

        return s;
    }

}
