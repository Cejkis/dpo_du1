package cz.fit.dpo.cbi;

import cz.fit.dpo.cbi.arithmetic.ArithmeticExpression;
import cz.fit.dpo.cbi.arithmetic.Director;
import cz.fit.dpo.cbi.arithmetic.RPNBuilder;

public class ArithmeticExpressionCreator {

    /**
     * Creates any expression from the RPN input. 
     *
     * @see //en.wikipedia.org/wiki/Reverse_Polish_notation
     *
     * @param input in Reverse Polish Notation
     * @return {@link ArithmeticExpression} equivalent of the RPN input.
     */
    public ArithmeticExpression createExpressionFromRPN(String input) {

        RPNBuilder builder = new RPNBuilder();
        Director dir = new Director(builder);

        dir.Construct(input);
        return builder.getResult();
    }
}
